WARNING:
This program EVIOCGRABs the input device, meaning all inputs from the device are handled by this program.

!! MAKE SURE YOU CAN CLOSE THE PROGRAM WITHOUT THE INPUT DEVICE !!

Note that you need to have another device to write to unless you are running this in X11 mode.

# How to compile:
## for evdev:
gcc main.c -O2
## for X11:
gcc main.c utf.c -O2 -lX11 -DX11
